# Diabètes gestationnel - Insuline et Metformine 

___


## Objectif

L'objectif du projet est d'identifier les symptômes des femmes enceintes atteintes de diabète gestationnel qui consomme de la metformine et/ou de l’insuline, à partir des posts recueillis sur un blog dédié aux femmes enceintes.

## Source de données 

Les données ont été recueillis sur le groupe **_Gestational Diabetes_** du forum anglophone **_What to expect_** dédié à la grossesse et à la parentalité. _(https://community.whattoexpect.com/forums/gestational-diabetes.html)_

Les données ont été collectés le 7 juin 2023.


## Matériel et méthode

Tous les scripts utilisés dans le cadre de ce projet sont disponibles dans le dossier **_scripts_**

Les fichiers suivants sont disponibles dans le dossier **_data_** :

| nom | contenu |
|----|----|
| link.csv | Lien vers chacun des posts du groupe _Gestational diabetes_ (GD) |
| gd_posts.csv | Ensemble des posts du groupe GD |
| metfo_gd_posts.csv | Ensemble des posts du groupe GD contenant le mot **metformin** |
| insu_gd_posts.csv | Ensemble des posts du groupe GD contenant le mot **insulin** |
| metfo_insu_gd_posts.csv | Ensemble des posts du groupe GD contenant au moins l'un des mots **metformin** et **insulin** |
| cleaned_posts.csv | Ensemble des posts du fichier _metfo_insu_gd_posts.csv_ après nettoyage | 


## Auteurs 

Narges Jafarzadeh et Chloé Saint-Dizier - Master Data Science en Santé, ILIS, Univeristé de Lille 

Supervisé par Naima Oubenali

## Mise à jour

Date de dernière modification : 09 juin 2023